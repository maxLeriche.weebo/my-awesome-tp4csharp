﻿namespace ClientTP4
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_addstudent = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_addpromo = new System.Windows.Forms.Button();
            this.btn_adddepart = new System.Windows.Forms.Button();
            this.btn_modif = new System.Windows.Forms.Button();
            this.datagrid = new System.Windows.Forms.DataGridView();
            this.btn_validate = new System.Windows.Forms.Button();
            this.indexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departementDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.promoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_addstudent
            // 
            this.btn_addstudent.Location = new System.Drawing.Point(592, 80);
            this.btn_addstudent.Name = "btn_addstudent";
            this.btn_addstudent.Size = new System.Drawing.Size(119, 23);
            this.btn_addstudent.TabIndex = 2;
            this.btn_addstudent.Text = "Ajout étudiant";
            this.btn_addstudent.UseVisualStyleBackColor = true;
            this.btn_addstudent.Click += new System.EventHandler(this.btn_addstudent_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(592, 109);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(119, 23);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Suppression étudiant";
            this.btn_delete.UseVisualStyleBackColor = true;
            // 
            // btn_addpromo
            // 
            this.btn_addpromo.Location = new System.Drawing.Point(592, 167);
            this.btn_addpromo.Name = "btn_addpromo";
            this.btn_addpromo.Size = new System.Drawing.Size(119, 23);
            this.btn_addpromo.TabIndex = 4;
            this.btn_addpromo.Text = "Ajout promotion";
            this.btn_addpromo.UseVisualStyleBackColor = true;
            this.btn_addpromo.Click += new System.EventHandler(this.btn_addpromo_Click);
            // 
            // btn_adddepart
            // 
            this.btn_adddepart.Location = new System.Drawing.Point(592, 196);
            this.btn_adddepart.Name = "btn_adddepart";
            this.btn_adddepart.Size = new System.Drawing.Size(119, 23);
            this.btn_adddepart.TabIndex = 5;
            this.btn_adddepart.Text = "Ajout département";
            this.btn_adddepart.UseVisualStyleBackColor = true;
            this.btn_adddepart.Click += new System.EventHandler(this.btn_adddepart_Click);
            // 
            // btn_modif
            // 
            this.btn_modif.Location = new System.Drawing.Point(592, 138);
            this.btn_modif.Name = "btn_modif";
            this.btn_modif.Size = new System.Drawing.Size(119, 23);
            this.btn_modif.TabIndex = 6;
            this.btn_modif.Text = "Modifier étudiant";
            this.btn_modif.UseVisualStyleBackColor = true;
            // 
            // datagrid
            // 
            this.datagrid.AccessibleName = "";
            this.datagrid.AllowUserToAddRows = false;
            this.datagrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.datagrid.AutoGenerateColumns = false;
            this.datagrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.indexDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.prenomDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.departementDataGridViewTextBoxColumn,
            this.promoDataGridViewTextBoxColumn});
            this.datagrid.DataSource = this.etudiantBindingSource;
            this.datagrid.Location = new System.Drawing.Point(12, 12);
            this.datagrid.Name = "datagrid";
            this.datagrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.datagrid.Size = new System.Drawing.Size(544, 390);
            this.datagrid.TabIndex = 7;
            // 
            // btn_validate
            // 
            this.btn_validate.Enabled = false;
            this.btn_validate.Location = new System.Drawing.Point(613, 225);
            this.btn_validate.Name = "btn_validate";
            this.btn_validate.Size = new System.Drawing.Size(75, 23);
            this.btn_validate.TabIndex = 8;
            this.btn_validate.Text = "Valider";
            this.btn_validate.UseVisualStyleBackColor = true;
            this.btn_validate.Click += new System.EventHandler(this.btn_validate_Click);
            // 
            // indexDataGridViewTextBoxColumn
            // 
            this.indexDataGridViewTextBoxColumn.DataPropertyName = "index";
            this.indexDataGridViewTextBoxColumn.HeaderText = "index";
            this.indexDataGridViewTextBoxColumn.Name = "indexDataGridViewTextBoxColumn";
            this.indexDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // prenomDataGridViewTextBoxColumn
            // 
            this.prenomDataGridViewTextBoxColumn.DataPropertyName = "prenom";
            this.prenomDataGridViewTextBoxColumn.HeaderText = "prenom";
            this.prenomDataGridViewTextBoxColumn.Name = "prenomDataGridViewTextBoxColumn";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DateDeNaissance";
            this.dataGridViewTextBoxColumn1.HeaderText = "DateDeNaissance";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // departementDataGridViewTextBoxColumn
            // 
            this.departementDataGridViewTextBoxColumn.DataPropertyName = "departement";
            this.departementDataGridViewTextBoxColumn.HeaderText = "departement";
            this.departementDataGridViewTextBoxColumn.Name = "departementDataGridViewTextBoxColumn";
            // 
            // promoDataGridViewTextBoxColumn
            // 
            this.promoDataGridViewTextBoxColumn.DataPropertyName = "promo";
            this.promoDataGridViewTextBoxColumn.HeaderText = "promo";
            this.promoDataGridViewTextBoxColumn.Name = "promoDataGridViewTextBoxColumn";
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataSource = typeof(ClientTP4.IutService.etudiant);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(592, 272);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 9;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(592, 298);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(716, 278);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Département";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(716, 305);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Promotion";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_validate);
            this.Controls.Add(this.datagrid);
            this.Controls.Add(this.btn_modif);
            this.Controls.Add(this.btn_adddepart);
            this.Controls.Add(this.btn_addpromo);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_addstudent);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.datagrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_addstudent;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_addpromo;
        private System.Windows.Forms.Button btn_adddepart;
        private System.Windows.Forms.Button btn_modif;
        private System.Windows.Forms.DataGridView datagrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDeNaissanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn indexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn departementDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn promoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btn_validate;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

