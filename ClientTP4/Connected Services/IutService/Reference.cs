﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientTP4.IutService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="etudiant", Namespace="http://schemas.datacontract.org/2004/07/WCFTP4")]
    [System.SerializableAttribute()]
    public partial class etudiant : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateDeNaissanceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string departementField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int indexField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nomField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string prenomField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string promoField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateDeNaissance {
            get {
                return this.DateDeNaissanceField;
            }
            set {
                if ((this.DateDeNaissanceField.Equals(value) != true)) {
                    this.DateDeNaissanceField = value;
                    this.RaisePropertyChanged("DateDeNaissance");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string departement {
            get {
                return this.departementField;
            }
            set {
                if ((object.ReferenceEquals(this.departementField, value) != true)) {
                    this.departementField = value;
                    this.RaisePropertyChanged("departement");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int index {
            get {
                return this.indexField;
            }
            set {
                if ((this.indexField.Equals(value) != true)) {
                    this.indexField = value;
                    this.RaisePropertyChanged("index");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string nom {
            get {
                return this.nomField;
            }
            set {
                if ((object.ReferenceEquals(this.nomField, value) != true)) {
                    this.nomField = value;
                    this.RaisePropertyChanged("nom");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string prenom {
            get {
                return this.prenomField;
            }
            set {
                if ((object.ReferenceEquals(this.prenomField, value) != true)) {
                    this.prenomField = value;
                    this.RaisePropertyChanged("prenom");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string promo {
            get {
                return this.promoField;
            }
            set {
                if ((object.ReferenceEquals(this.promoField, value) != true)) {
                    this.promoField = value;
                    this.RaisePropertyChanged("promo");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="IutService.IIUT")]
    public interface IIUT {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreateDepartement", ReplyAction="http://tempuri.org/IIUT/CreateDepartementResponse")]
        bool CreateDepartement(string p_NomDepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreateDepartement", ReplyAction="http://tempuri.org/IIUT/CreateDepartementResponse")]
        System.Threading.Tasks.Task<bool> CreateDepartementAsync(string p_NomDepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreatePromotion", ReplyAction="http://tempuri.org/IIUT/CreatePromotionResponse")]
        bool CreatePromotion(string p_NomDepartement, string p_NomPromo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreatePromotion", ReplyAction="http://tempuri.org/IIUT/CreatePromotionResponse")]
        System.Threading.Tasks.Task<bool> CreatePromotionAsync(string p_NomDepartement, string p_NomPromo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreateStudent", ReplyAction="http://tempuri.org/IIUT/CreateStudentResponse")]
        bool CreateStudent(string p_prenom, string p_nom, System.DateTime p_date, string p_nomdepartement, string p_nompromotion);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/CreateStudent", ReplyAction="http://tempuri.org/IIUT/CreateStudentResponse")]
        System.Threading.Tasks.Task<bool> CreateStudentAsync(string p_prenom, string p_nom, System.DateTime p_date, string p_nomdepartement, string p_nompromotion);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/UpdateStudent", ReplyAction="http://tempuri.org/IIUT/UpdateStudentResponse")]
        string UpdateStudent(int p_index, string p_nom, string p_prenom, System.DateTime p_birthdate, string p_nompromo, string p_nomdepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/UpdateStudent", ReplyAction="http://tempuri.org/IIUT/UpdateStudentResponse")]
        System.Threading.Tasks.Task<string> UpdateStudentAsync(int p_index, string p_nom, string p_prenom, System.DateTime p_birthdate, string p_nompromo, string p_nomdepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindAllStudent", ReplyAction="http://tempuri.org/IIUT/FindAllStudentResponse")]
        ClientTP4.IutService.etudiant[] FindAllStudent();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindAllStudent", ReplyAction="http://tempuri.org/IIUT/FindAllStudentResponse")]
        System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindAllStudentAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyDepartement", ReplyAction="http://tempuri.org/IIUT/FindStudentbyDepartementResponse")]
        ClientTP4.IutService.etudiant[] FindStudentbyDepartement(string p_nomDepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyDepartement", ReplyAction="http://tempuri.org/IIUT/FindStudentbyDepartementResponse")]
        System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyDepartementAsync(string p_nomDepartement);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyPromotion", ReplyAction="http://tempuri.org/IIUT/FindStudentbyPromotionResponse")]
        ClientTP4.IutService.etudiant[] FindStudentbyPromotion(string p_nompromotion);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyPromotion", ReplyAction="http://tempuri.org/IIUT/FindStudentbyPromotionResponse")]
        System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyPromotionAsync(string p_nompromotion);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyName", ReplyAction="http://tempuri.org/IIUT/FindStudentbyNameResponse")]
        ClientTP4.IutService.etudiant[] FindStudentbyName(string p_nometudiant);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyName", ReplyAction="http://tempuri.org/IIUT/FindStudentbyNameResponse")]
        System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyNameAsync(string p_nometudiant);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyAge", ReplyAction="http://tempuri.org/IIUT/FindStudentbyAgeResponse")]
        ClientTP4.IutService.etudiant[] FindStudentbyAge(System.DateTime p_birthdate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IIUT/FindStudentbyAge", ReplyAction="http://tempuri.org/IIUT/FindStudentbyAgeResponse")]
        System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyAgeAsync(System.DateTime p_birthdate);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IIUTChannel : ClientTP4.IutService.IIUT, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class IUTClient : System.ServiceModel.ClientBase<ClientTP4.IutService.IIUT>, ClientTP4.IutService.IIUT {
        
        public IUTClient() {
        }
        
        public IUTClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public IUTClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IUTClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public IUTClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool CreateDepartement(string p_NomDepartement) {
            return base.Channel.CreateDepartement(p_NomDepartement);
        }
        
        public System.Threading.Tasks.Task<bool> CreateDepartementAsync(string p_NomDepartement) {
            return base.Channel.CreateDepartementAsync(p_NomDepartement);
        }
        
        public bool CreatePromotion(string p_NomDepartement, string p_NomPromo) {
            return base.Channel.CreatePromotion(p_NomDepartement, p_NomPromo);
        }
        
        public System.Threading.Tasks.Task<bool> CreatePromotionAsync(string p_NomDepartement, string p_NomPromo) {
            return base.Channel.CreatePromotionAsync(p_NomDepartement, p_NomPromo);
        }
        
        public bool CreateStudent(string p_prenom, string p_nom, System.DateTime p_date, string p_nomdepartement, string p_nompromotion) {
            return base.Channel.CreateStudent(p_prenom, p_nom, p_date, p_nomdepartement, p_nompromotion);
        }
        
        public System.Threading.Tasks.Task<bool> CreateStudentAsync(string p_prenom, string p_nom, System.DateTime p_date, string p_nomdepartement, string p_nompromotion) {
            return base.Channel.CreateStudentAsync(p_prenom, p_nom, p_date, p_nomdepartement, p_nompromotion);
        }
        
        public string UpdateStudent(int p_index, string p_nom, string p_prenom, System.DateTime p_birthdate, string p_nompromo, string p_nomdepartement) {
            return base.Channel.UpdateStudent(p_index, p_nom, p_prenom, p_birthdate, p_nompromo, p_nomdepartement);
        }
        
        public System.Threading.Tasks.Task<string> UpdateStudentAsync(int p_index, string p_nom, string p_prenom, System.DateTime p_birthdate, string p_nompromo, string p_nomdepartement) {
            return base.Channel.UpdateStudentAsync(p_index, p_nom, p_prenom, p_birthdate, p_nompromo, p_nomdepartement);
        }
        
        public ClientTP4.IutService.etudiant[] FindAllStudent() {
            return base.Channel.FindAllStudent();
        }
        
        public System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindAllStudentAsync() {
            return base.Channel.FindAllStudentAsync();
        }
        
        public ClientTP4.IutService.etudiant[] FindStudentbyDepartement(string p_nomDepartement) {
            return base.Channel.FindStudentbyDepartement(p_nomDepartement);
        }
        
        public System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyDepartementAsync(string p_nomDepartement) {
            return base.Channel.FindStudentbyDepartementAsync(p_nomDepartement);
        }
        
        public ClientTP4.IutService.etudiant[] FindStudentbyPromotion(string p_nompromotion) {
            return base.Channel.FindStudentbyPromotion(p_nompromotion);
        }
        
        public System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyPromotionAsync(string p_nompromotion) {
            return base.Channel.FindStudentbyPromotionAsync(p_nompromotion);
        }
        
        public ClientTP4.IutService.etudiant[] FindStudentbyName(string p_nometudiant) {
            return base.Channel.FindStudentbyName(p_nometudiant);
        }
        
        public System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyNameAsync(string p_nometudiant) {
            return base.Channel.FindStudentbyNameAsync(p_nometudiant);
        }
        
        public ClientTP4.IutService.etudiant[] FindStudentbyAge(System.DateTime p_birthdate) {
            return base.Channel.FindStudentbyAge(p_birthdate);
        }
        
        public System.Threading.Tasks.Task<ClientTP4.IutService.etudiant[]> FindStudentbyAgeAsync(System.DateTime p_birthdate) {
            return base.Channel.FindStudentbyAgeAsync(p_birthdate);
        }
    }
}
