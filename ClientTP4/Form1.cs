﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientTP4
{
    public partial class Form1 : Form
    {
        IutService.IUTClient v_serv = new IutService.IUTClient();
        public Form1()
        {
            InitializeComponent();
        }
        private void btn_addstudent_Click(object sender, EventArgs e)
        {
            bool m_value = true;
            datagrid.AllowUserToAddRows = m_value;
            btn_validate.Enabled = m_value;
        }

        private void btn_validate_Click(object sender, EventArgs e)
        {
            if (textBox1.Enabled)
            {
                btn_adddepart_Click(sender, e);
                btn_addpromo_Click(sender, e);
            }
            else
            {
                int m_index = 0;
                string m_name = datagrid.Columns[m_index].ToString();
                string m_prenom = datagrid.Columns[m_index + 1].ToString();
                string[] datestring = datagrid.Columns[m_index + 2].ToString().Split('/');
                DateTime m_date = new DateTime(Int32.Parse(datestring[0]), Int32.Parse(datestring[1]), Int32.Parse(datestring[2]));
                string m_depart = datagrid.Columns[m_index + 3].ToString();
                string m_promo = datagrid.Columns[m_index + 4].ToString();
                v_serv.CreateStudent(m_name, m_prenom, m_date, m_depart, m_promo);
            }
    

        }

        private void btn_addpromo_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            label1.Enabled = true;
            label2.Enabled = true;
            btn_validate.Enabled = true;
        }

        private void btn_adddepart_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            label1.Enabled = true;
            btn_validate.Enabled = true;
        }
    }
}
