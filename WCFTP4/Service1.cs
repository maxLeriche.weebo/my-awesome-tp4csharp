﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFTP4
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Service1" à la fois dans le code et le fichier de configuration.
    
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Service1 : IIUT
    {
        List<Departement> IUT = new List<Departement>();

        #region Creation
        public bool CreateDepartement(string p_NomDepartement)
        {
            if (string.IsNullOrEmpty(p_NomDepartement))
            {
                throw new ArgumentException("departement invalide", nameof(p_NomDepartement));
            }
            if(findindexdepartement(p_NomDepartement)!=-1)
            {
                throw new Exception("Departement déja existant");
            }

            IUT.Add(new Departement(p_NomDepartement));
            return true;
        }

        public bool CreatePromotion(string p_NomDepartement, string p_NomPromo)
        {
            if (string.IsNullOrEmpty(p_NomDepartement))
            {
                throw new ArgumentException("departement invalide", nameof(p_NomDepartement));
            }

            if (string.IsNullOrEmpty(p_NomPromo))
            {
                throw new ArgumentException("promo invalide", nameof(p_NomPromo));
            }
            try
            {
                IUT[findindexdepartement(p_NomDepartement)].CreatePromo(p_NomPromo);
                return true;
            }
            catch(Exception ee)
            {
                throw ee;
            }
            
        }

        public bool CreateStudent(string p_prenom, string p_nom, DateTime p_date, string p_nomdepartement, string p_nompromotion)
        {
            if (string.IsNullOrEmpty(p_prenom))
            {
                throw new ArgumentException("prenom invalude", nameof(p_prenom));
            }

            if (string.IsNullOrEmpty(p_nom))
            {
                throw new ArgumentException("nom invalide", nameof(p_nom));
            }

            if (string.IsNullOrEmpty(p_nomdepartement))
            {
                throw new ArgumentException("departement invalide", nameof(p_nomdepartement));
            }

            if (string.IsNullOrEmpty(p_nompromotion))
            {
                throw new ArgumentException("promo invalide", nameof(p_nompromotion));
            }

            if(DateTime.Now<p_date)
            {
                throw new Exception("Date dans le futur");
            }
            try
            {
                etudiant m_etudiant = new etudiant(p_prenom, p_nom, p_date);
                int indexdepartement = findindexdepartement(p_nomdepartement);
                IUT[indexdepartement].ListPromo[findindexpromoindepartement(p_nompromotion, indexdepartement)].AddStudent(m_etudiant);
            }
            catch(Exception ee)
            {
                throw ee;
            }
            
            return true;
        }

        #endregion

        #region update
        public string UpdateStudent(int p_index, string p_nom, string p_prenom, DateTime p_birthdate, string p_nompromo, string p_nomdepartement)
        {
            string ValeurdeRetour = "";
            if (string.IsNullOrEmpty(p_nom))
            {
                ValeurdeRetour += "Pas de nom préciser |";
            }

            if (string.IsNullOrEmpty(p_prenom))
            {
                ValeurdeRetour += "pas de prénom préciser |";
            }

            if (string.IsNullOrEmpty(p_nompromo))
            {
                ValeurdeRetour += "Pas de promo préciser |";
            }

            if (string.IsNullOrEmpty(p_nomdepartement))
            {
                ValeurdeRetour += "pas de département préciser |";
            }

            if (DateTime.Now < p_birthdate)
            {
                throw new Exception("Date dans le futur");
            }

            if (ValeurdeRetour== "")
            {
                int m_indexdepartement = findindexdepartement(p_nomdepartement);
                if(m_indexdepartement!=-1)
                {
                    int m_indexprommo = findindexpromoindepartement(p_nompromo, m_indexdepartement);
                    if (m_indexprommo!=-1)
                    {
                        int m_indexetudiant = findindexstudentintopromoetdepartement(p_index, m_indexprommo, m_indexdepartement);
                        if(m_indexetudiant!=-1)
                        {
                            IUT[m_indexdepartement].ListPromo[m_indexprommo].MembrePromo[m_indexetudiant].updateetudiant(p_prenom, p_nom, p_birthdate);
                        }
                        else
                        {
                            ValeurdeRetour += "Etudiant inexistant";
                        }
                    }
                    else
                    {
                        ValeurdeRetour += "Promo inexistant";
                    }
                }
                else
                {
                    ValeurdeRetour += "Departement inexistant";
                    ValeurdeRetour += m_indexdepartement;
                }
            }

            if(ValeurdeRetour!="")
            {
                throw new Exception(ValeurdeRetour);
            }

            return ValeurdeRetour;
        }

        #endregion

        #region findindex

        private int findindexdepartement(string p_nomdepartement)
        {
            return IUT.FindIndex((departement) =>
            {
                if (departement.NomDepartement == p_nomdepartement) return true;
                return false;
            });
        }

        private int findindexpromoindepartement(string p_nompromo,int p_departement)
        {
            return IUT[p_departement].ListPromo
                .FindIndex((promo) =>
            {
                if (promo.NomdePromo == p_nompromo) return true;
                return false;
            });
        }
        
        private int findindexstudentintopromoetdepartement(int p_index,int p_promo,int p_departement)
        {
            return IUT[p_departement].ListPromo[p_promo].MembrePromo
                 .FindIndex((student) =>
                 {
                     if (student.index == p_index) return true;
                     return false;
                 });
        }
        
        #endregion

        #region recherche

        

        public List<etudiant> FindStudentbyAge(DateTime p_birthdate)
        {
            List<etudiant> retourner = new List<etudiant>();

            IUT.ForEach(delegate (Departement p_current)
            {
                p_current.ListPromo.ForEach((p_currentpromo) =>
                {
                    p_currentpromo.MembrePromo.FindAll((m_p_currentstudent) =>
                    {
                        if (m_p_currentstudent.DateDeNaissance==p_birthdate)
                        {
                            retourner.Add(m_p_currentstudent);
                            return true;
                        }
                        return false;
                    });
                });
            });
            return retourner;
        }

        public List<etudiant> FindStudentbyDepartement(string p_nomDepartement)
        {
            List<etudiant> retourner = new List<etudiant>();

            IUT.ForEach(delegate (Departement p_current)
            {
                if(p_current.NomDepartement==p_nomDepartement)
                {
                    p_current.ListPromo.ForEach((p_promo) =>
                    {
                        retourner.AddRange(p_promo.MembrePromo);
                    });
                }
            });
            return retourner;
        }

        public List<etudiant> FindStudentbyName(string p_nometudiant)
        {
            List<etudiant> retourner = new List<etudiant>();

            IUT.ForEach(delegate (Departement p_current)
            {
                p_current.ListPromo.ForEach((p_currentpromo) =>
                {
                    p_currentpromo.MembrePromo.FindAll((p_currentstudent) =>
                    {
                        if (p_currentstudent.nom.StartsWith(p_nometudiant))
                        {
                            retourner.Add(p_currentstudent);
                            return true;
                        }
                        return false;
                    });
                });
            });
            return retourner;
        }

        public List<etudiant> FindStudentbyPromotion(string p_nompromotion)
        {
            List<etudiant> retourner = new List<etudiant>();

            IUT.ForEach(delegate (Departement p_current)
            {
                retourner.AddRange(p_current.ListPromo.Find((p_promo) =>
                {
                    if (p_promo.NomdePromo == p_nompromotion) { return true; }
                    return false;
                }).MembrePromo);
            });
            return retourner;
        }

        public List<etudiant> FindAllStudent()
        {
            List<etudiant> retourner = new List<etudiant>();

            IUT.ForEach(delegate (Departement p_current)
            {
                p_current.ListPromo.ForEach((p_promo) =>
                {
                    retourner.AddRange(p_promo.MembrePromo);
                });
            });
            return retourner;
        }

        public List<string> FindALLPromo()
        {
            List<string> nompromo = new List<string>();

            IUT.ForEach(delegate (Departement p_current)
            {
                p_current.ListPromo.ForEach((p_currentpromo) =>
                {
                    nompromo.Add(p_currentpromo.NomdePromo);
                });
            });
            return nompromo;
        }

        public List<string> FindAllDepartement()
        {
            List<string> nomdepartement = new List<string>();

            IUT.ForEach(delegate (Departement p_current)
            {
                nomdepartement.Add(p_current.NomDepartement);
            });
            return nomdepartement;
        }

        #endregion

    }
}
