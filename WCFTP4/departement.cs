﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFTP4
{
    /// <summary>
    /// Class représentant un département de l'iut
    /// </summary>
    [DataContract]
    public class Departement
    {
        
        private string m_nomDepartement;
        private List<Promotion> m_nbrdepromo = new List<Promotion>();

        /// <summary>
        /// constructeur de département (sans directeur =D)
        /// </summary>
        /// <param name="p_nomDepartement">Nom du département</param>
        public Departement(string p_nomDepartement)
        {
            m_nomDepartement = p_nomDepartement;
        }

        /// <summary>
        /// Ajout d'une promo aux département
        /// </summary>
        /// <param name="p_promo">Un object promo préconfigurer</param>
        /// <returns>Si oui ou non tout c'est bien passer</returns>
        private bool Addpromo(Promotion p_promo)
        {
            m_nbrdepromo.Find((p_current) =>
            {
                if (p_current.NomdePromo == p_promo.NomdePromo) throw new Exception("Nom de promo déja existant");
                return false;
            });
            m_nbrdepromo.Add(p_promo);
            return true;
        }
        /// <summary>
        ///  Creer un object promo qui sera ajouter via ADDPromo
        /// </summary>
        /// <param name="p_nomdepromo">Si oui ou non tout c'est bien passer lors de la création + ajout</param>
        /// <returns></returns>
        public bool CreatePromo(string p_nomdepromo)
        {
            try
            {
                return Addpromo(new Promotion(p_nomdepromo, m_nomDepartement));
            }
            catch (Exception ee)
            {
                throw ee;
            }
            
        }

        /// <summary>
        /// Get du nom de département
        /// </summary>
        [DataMember]
        public string NomDepartement
        {
            get { return m_nomDepartement; }
        }

        /// <summary>
        /// Get de la list des promotion
        /// </summary>
        [DataMember]
        public List<Promotion> ListPromo
        {
            get { return m_nbrdepromo; }
        }
    }
}
