﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace WCFTP4
{
    [DataContract]
    public class etudiant
    {
        private int m_index;
        private string m_prenom;
        private string m_nom;
        private DateTime m_date = new DateTime();
        private string m_NomDepartement;
        private string m_NomPromotion;

        public etudiant(string p_prenom, string p_nom, DateTime p_date)
        {
            m_prenom = p_prenom;
            m_nom = p_nom;
            m_date = p_date;
            
        }

        private etudiant(string p_prenom, string p_nom, DateTime p_date, string p_promo, string p_departement, int p_index)
        {
            m_prenom = p_prenom;
            m_nom = p_nom;
            m_date = p_date;
            m_NomDepartement = p_departement;
            m_NomPromotion = p_promo;
            m_index = p_index;
        }

        public void updateetudiant(string p_prenom, string p_nom, DateTime p_date)
        {
            m_prenom = p_prenom;
            m_nom = p_nom;
            m_date = p_date;
        }

        [DataMember]
        public int index
        {
            get { return m_index; }
            set { m_index = value; }
        }

        [DataMember]
        public string prenom
        {
            get { return m_prenom; }
            set { m_prenom = value; }
        }

        [DataMember]
        public string nom
        {
            get { return m_nom; }
            set { m_nom = value; }
        }

        [DataMember]
        public DateTime DateDeNaissance
        {
            get { return m_date; }
            set { m_date = value; }
        }

        [DataMember]
        public string promo
        {
            get { return m_NomPromotion; }
            set { m_NomPromotion = value; }
        }

        [DataMember]
        public string departement
        {
            get { return m_NomDepartement; }
            set { m_NomDepartement = value; }
        }


    }

}
