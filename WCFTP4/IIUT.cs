﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFTP4
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IIUT" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IIUT
    {
        [OperationContract]
        bool CreateDepartement(string p_NomDepartement);

        [OperationContract]
        bool CreatePromotion(string p_NomDepartement,string p_NomPromo);

        [OperationContract]
        bool CreateStudent(string p_prenom, string p_nom, DateTime p_date, string p_nomdepartement,string p_nompromotion);

        [OperationContract]
        string UpdateStudent(int p_index, string p_nom, string p_prenom, DateTime p_birthdate, string p_nompromo, string p_nomdepartement);

        [OperationContract]
        List<etudiant> FindAllStudent();

        [OperationContract]
        List<etudiant> FindStudentbyDepartement(string p_nomDepartement);

        [OperationContract]
        List<string> FindALLPromo();

        [OperationContract]
        List<string> FindAllDepartement();

        [OperationContract]
        List<etudiant> FindStudentbyPromotion(string p_nompromotion);

        [OperationContract]
        List<etudiant> FindStudentbyName(string p_nometudiant);

        [OperationContract]
        List<etudiant> FindStudentbyAge(DateTime p_birthdate);
    }
}
