﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFTP4
{
    /// <summary>
    /// Class représentant une promotion
    /// </summary>
    [DataContract]
    public class Promotion
    {
        private int indexvaluestudent = 0;
        private string m_nomdedepartement;
        private string m_nomdepromo;
        private List<etudiant> m_membrePromo = new List<etudiant>();

        public Promotion(string p_nomdepromo, string p_nomdedepartement)
        {
            m_nomdepromo = p_nomdepromo;
            m_nomdedepartement = p_nomdedepartement;
        }

        public bool AddStudent(etudiant p_etudiant)
        {
            try
            {
                p_etudiant.departement = m_nomdedepartement;
                p_etudiant.promo = m_nomdepromo;
                p_etudiant.index = indexvaluestudent;
                this.indexvaluestudent += 1;
                m_membrePromo.Add(p_etudiant);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool CreateStudent(string p_prenom, string p_nom, DateTime p_date)
        {
            return AddStudent(new etudiant(p_prenom, p_nom, p_date));
        }

        [DataMember]
        public string NomdePromo
        {
            get { return m_nomdepromo; }
            set { m_nomdepromo = value; }
        }

        [DataMember]
        public List<etudiant> MembrePromo
        {
            get { return m_membrePromo; }
            set { m_membrePromo = value; }
        }
    }
}
